/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:33:01 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 19:37:37 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_H
# define PRINT_H

# include <unistd.h>
# include "map.h"
# include "piece.h"
# include "libft.h"

void	print_map(t_map *map);
void	print_piece(t_piece *piece);
void	usage(char *cmd);
void	msg_exit(char *msg);

#endif
