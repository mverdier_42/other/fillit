/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:27:29 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/10 12:30:35 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H
# define MAP_H

# include <stdlib.h>
# include "libft.h"

# define EMPTY_SYM '.'

typedef struct	s_map
{
	int		max_size;
	int		size;
	char	**grid;
}				t_map;

t_map			*create_map(int max_size);
void			delete_map(t_map *map);
void			init_map(t_map *map);
int				map_size(t_map *map);
void			map_cpy(t_map *dest, t_map *src);

#endif
