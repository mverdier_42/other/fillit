/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:32:23 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 19:28:19 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SOLVE_H
# define SOLVE_H

# include "map.h"
# include "piece.h"
# include "map_utils.h"
# include "ft_plst.h"

t_map	*solve(t_plst *pieces);

/*
** Intern Fonction
*/

void	solve_r(t_plst_e *pieces, t_map *map, t_map *best);

#endif
