/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_tools.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 12:52:15 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/11 15:57:22 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIECE_TOOLS_H
# define PIECE_TOOLS_H

# include "piece.h"
# include "input.h"
# include "libft.h"

void	move_pieces(t_plst *pieces);

#endif
