/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 19:14:24 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 19:14:25 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H

# include "piece.h"
# include "libft.h"

# include "piece_size.h"
# include "piece_tools.h"
# include "valid.h"

# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>

typedef enum	e_savep
{
	BAD,
	GOOD,
	END,
	END_EMPTY
}				t_savep;

t_plst	*read_input(char *file);

#endif
