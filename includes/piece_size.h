/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_size.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:32:08 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/11 15:57:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIECE_SIZE_H
# define PIECE_SIZE_H

# include "piece.h"
# include "libft.h"

void	piece_size(t_plst *pieces);

#endif
