/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:26:07 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 16:27:36 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIECE_H
# define PIECE_H

# include <stdlib.h>

# define PIECE_MAX_W 4

typedef enum	e_cell
{
	EMPTY,
	FULL,
	CHECKED
}				t_cell;

typedef struct	s_piece
{
	t_cell	**grid;
	int		w;
	int		h;
	char	sym;
}				t_piece;

t_piece			*create_piece(void);
void			delete_piece(t_piece *p);

#endif
