/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_utils.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:32:17 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/10 11:07:19 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_UTILS_H
# define MAP_UTILS_H

# include "map.h"
# include "piece.h"
# include "libft.h"

void	print_on_map(t_piece *p, int x, int y, t_map *map);
void	erase_on_map(t_piece *p, int x, int y, t_map *map);
t_bool	can_put_on_map(t_piece *p, int x, int y, t_map *map);

#endif
