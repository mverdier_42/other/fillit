/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 13:21:57 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 20:00:46 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "solve.h"
#include "input.h"
#include "print.h"

t_bool	is_valid_void(void *piece)
{
	return (is_valid(piece));
}

void	delete_piece_void(void *piece)
{
	delete_piece(piece);
}

int		main(int argc, char **argv)
{
	t_plst	*list;
	t_map	*map;

	if (argc != 2)
	{
		usage(argv[0]);
		return (0);
	}
	if ((list = read_input(argv[1])) == NULL)
		msg_exit("error\n");
	if (ft_plst_count_if(list, &is_valid_void) != list->size)
	{
		ft_plst_delete_f(list, &delete_piece_void);
		msg_exit("error\n");
	}
	if ((map = solve(list)) == NULL)
	{
		ft_plst_delete_f(list, &delete_piece_void);
		msg_exit("solve error\n");
	}
	print_map(map);
	delete_map(map);
	ft_plst_delete_f(list, &delete_piece_void);
	return (0);
}
