/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:28:37 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/07 12:28:38 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

t_piece	*create_piece(void)
{
	t_piece	*ret;
	int		i;

	if ((ret = malloc(sizeof(*ret))) == NULL)
		return (NULL);
	if ((ret->grid = malloc(sizeof(*(ret->grid)) * PIECE_MAX_W)) == NULL)
		return (NULL);
	i = 0;
	while (i < PIECE_MAX_W)
	{
		if ((ret->grid[i] =
					malloc(sizeof(*(ret->grid[i])) * PIECE_MAX_W)) == NULL)
			return (NULL);
		i++;
	}
	return (ret);
}

void	delete_piece(t_piece *p)
{
	int	i;

	if (p == NULL)
		return ;
	i = 0;
	while (i < PIECE_MAX_W)
	{
		free(p->grid[i]);
		i++;
	}
	free(p->grid);
	free(p);
}
