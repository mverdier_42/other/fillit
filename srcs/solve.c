/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:42:21 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/15 16:25:20 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "solve.h"

static void	solve_x(t_plst_e *pieces, int y, t_map *map, t_map *best)
{
	int	x;

	x = 0;
	while (x + ((t_piece *)pieces->content)->w < best->size)
	{
		if (can_put_on_map((t_piece *)pieces->content, x, y, map))
		{
			print_on_map((t_piece *)pieces->content, x, y, map);
			solve_r(pieces->next, map, best);
			erase_on_map((t_piece *)pieces->content, x, y, map);
		}
		x++;
	}
}

static void	solve_y(t_plst_e *pieces, t_map *map, t_map *best)
{
	int	y;

	y = 0;
	while (y + ((t_piece *)pieces->content)->h < best->size)
	{
		solve_x(pieces, y, map, best);
		y++;
	}
}

void		solve_r(t_plst_e *pieces, t_map *map, t_map *best)
{
	if (pieces == NULL)
	{
		if (map->size < best->size)
			map_cpy(best, map);
		return ;
	}
	solve_y(pieces, map, best);
}

t_map		*solve(t_plst *pieces)
{
	t_map	*map;
	t_map	*best;

	if ((map = create_map(pieces->size * PIECE_MAX_W + 1)) == NULL)
		return (NULL);
	if ((best = create_map(pieces->size * PIECE_MAX_W + 1)) == NULL)
		return (NULL);
	init_map(map);
	best->size = best->max_size;
	solve_r(pieces->first, map, best);
	delete_map(map);
	return (best);
}
