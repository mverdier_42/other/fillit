/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:10:23 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/11 19:24:48 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece_size.h"

static void	find_size(t_piece *piece)
{
	int i;
	int j;

	piece->w = 0;
	piece->h = 0;
	i = 0;
	while (i < PIECE_MAX_W)
	{
		j = 0;
		while (j < PIECE_MAX_W)
		{
			if (piece->grid[i][j] == FULL)
			{
				piece->w = MAX(piece->w, i + 1);
				piece->h = MAX(piece->h, j + 1);
			}
			j++;
		}
		i++;
	}
}

void		piece_size(t_plst *pieces)
{
	t_plst_e	*tmp;
	t_piece		*piece;

	tmp = pieces->first;
	while (tmp != NULL)
	{
		piece = (t_piece *)tmp->content;
		find_size(piece);
		tmp = tmp->next;
	}
}
