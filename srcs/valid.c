/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 11:20:01 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/11 19:03:49 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "valid.h"

static void	fill_checked(t_piece *piece, int x, int y)
{
	if (x < 0 || y < 0 || x > PIECE_MAX_W - 1 || y > PIECE_MAX_W - 1)
		return ;
	if (piece->grid[x][y] == EMPTY || piece->grid[x][y] == CHECKED)
		return ;
	piece->grid[x][y] = CHECKED;
	fill_checked(piece, x + 1, y);
	fill_checked(piece, x - 1, y);
	fill_checked(piece, x, y + 1);
	fill_checked(piece, x, y - 1);
}

static int	valid_core(t_piece *piece)
{
	int	i;
	int	j;
	int	n;

	n = 0;
	i = 0;
	while (i < PIECE_MAX_W)
	{
		j = 0;
		while (j < PIECE_MAX_W)
		{
			if (piece->grid[i][j] == FULL)
				return (-1);
			if (piece->grid[i][j] == CHECKED)
			{
				piece->grid[i][j] = FULL;
				n++;
			}
			j++;
		}
		i++;
	}
	return (n);
}

t_bool		is_valid(t_piece *piece)
{
	int	i;

	i = 0;
	while (i < PIECE_MAX_W && piece->grid[i][0] != FULL)
		i++;
	if (i == PIECE_MAX_W)
		return (FALSE);
	fill_checked(piece, i, 0);
	return (valid_core(piece) == PIECE_MAX_W);
}
