/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:27:21 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 19:38:41 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print.h"

void	print_map(t_map *map)
{
	int i;
	int j;

	i = 0;
	while (i < map->size)
	{
		j = 0;
		while (j < map->size)
		{
			ft_putchar(map->grid[j][i]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void	print_piece(t_piece *piece)
{
	int	i;
	int	j;

	ft_putstr("piece->w: ");
	ft_putnbr(piece->w);
	ft_putchar('\n');
	ft_putstr("piece->h: ");
	ft_putnbr(piece->h);
	ft_putchar('\n');
	i = 0;
	while (i < piece->h)
	{
		j = 0;
		while (j < piece->w)
		{
			if (piece->grid[j][i] == FULL)
				ft_putchar(piece->sym);
			else
				ft_putchar(EMPTY_SYM);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void	usage(char *cmd)
{
	ft_putstr("usage: ");
	ft_putstr(cmd);
	ft_putstr(" file\n");
}

void	msg_exit(char *msg)
{
	ft_putstr(msg);
	exit(1);
}
