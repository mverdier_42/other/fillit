/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 12:01:23 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/15 16:24:34 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static t_savep	look_grid(char *buf, t_piece *piece, int i)
{
	int j;

	j = 0;
	while (j < PIECE_MAX_W)
	{
		if (buf[j] == '#')
			piece->grid[j][i] = FULL;
		else if (buf[j] == '.')
			piece->grid[j][i] = EMPTY;
		else
			return (BAD);
		j++;
	}
	return (GOOD);
}

static t_savep	look_piece(int fd, t_piece *piece)
{
	int		ret;
	char	buf[PIECE_MAX_W + 1];
	int		i;
	t_savep	tmp;

	i = 0;
	while (i < PIECE_MAX_W)
	{
		ret = read(fd, buf, PIECE_MAX_W + 1);
		if (ret == 0)
		{
			if (i == 0)
				return (END_EMPTY);
			return (BAD);
		}
		if (ret < PIECE_MAX_W + 1)
			return (BAD);
		if ((tmp = look_grid(buf, piece, i)) == BAD)
			return (BAD);
		else if (buf[PIECE_MAX_W] != '\n')
			return (BAD);
		i++;
	}
	return (GOOD);
}

static t_savep	save_piece(int fd, char sym, t_plst *pieces)
{
	t_piece	*piece;
	t_savep	tmp;
	char	buf[PIECE_MAX_W + 1];
	int		ret;

	if ((piece = create_piece()) == NULL)
		return (BAD);
	piece->sym = sym;
	if ((tmp = look_piece(fd, piece)) != GOOD)
		return (tmp);
	ret = read(fd, buf, 1);
	if (ret < 0)
		return (BAD);
	if (ret == 0)
	{
		pieces = ft_plst_push_back(pieces, piece);
		return (END);
	}
	if (buf[0] != '\n')
		return (BAD);
	pieces = ft_plst_push_back(pieces, piece);
	return (GOOD);
}

t_plst			*read_input(char *file)
{
	int		fd;
	char	sym;
	t_plst	*pieces;
	t_savep	tmp;

	sym = 'A';
	if ((pieces = ft_plst_new()) == NULL)
		return (NULL);
	if ((fd = open(file, O_RDONLY)) < 0)
		return (NULL);
	while ((tmp = save_piece(fd, sym++, pieces)) != END)
	{
		if (tmp == END_EMPTY)
			return (NULL);
		if (tmp == BAD)
			return (NULL);
	}
	move_pieces(pieces);
	piece_size(pieces);
	close(fd);
	return (pieces);
}
