/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 12:52:13 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/11 19:19:56 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece_tools.h"

static t_bool	move_left(t_piece *piece)
{
	int		i;
	int		j;
	t_cell	tmp;

	i = 0;
	while (i < PIECE_MAX_W)
	{
		if (piece->grid[i++][0] == FULL)
			return (FALSE);
	}
	i = 0;
	while (i < PIECE_MAX_W)
	{
		tmp = piece->grid[i][0];
		j = 0;
		while (j + 1 <= PIECE_MAX_W - 1)
		{
			piece->grid[i][j] = piece->grid[i][j + 1];
			j++;
		}
		piece->grid[i][j] = tmp;
		i++;
	}
	return (TRUE);
}

static t_bool	move_up(t_piece *piece)
{
	int		i;
	int		j;
	t_cell	tmp;

	j = 0;
	while (j < PIECE_MAX_W)
	{
		if (piece->grid[0][j++] == FULL)
			return (FALSE);
	}
	j = 0;
	while (j < PIECE_MAX_W)
	{
		i = 0;
		tmp = piece->grid[0][j];
		while (i + 1 <= PIECE_MAX_W - 1)
		{
			piece->grid[i][j] = piece->grid[i + 1][j];
			i++;
		}
		piece->grid[i][j] = tmp;
		j++;
	}
	return (TRUE);
}

void			move_pieces(t_plst *pieces)
{
	t_plst_e	*tmp;
	t_piece		*piece;
	int			i;

	tmp = pieces->first;
	while (tmp != NULL)
	{
		i = 0;
		piece = (t_piece *)tmp->content;
		while (move_up(piece) == TRUE && i < PIECE_MAX_W)
			i++;
		i = 0;
		while (move_left(piece) == TRUE && i < PIECE_MAX_W)
			i++;
		tmp = tmp->next;
	}
}
