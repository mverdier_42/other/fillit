/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:25:40 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/11 19:24:26 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_utils.h"

void	print_on_map(t_piece *p, int x, int y, t_map *map)
{
	int	i;
	int	j;
	int	maxj;

	maxj = 0;
	i = 0;
	while (i < p->w)
	{
		j = 0;
		while (j < p->h)
		{
			if (p->grid[i][j] == FULL)
				map->grid[x + i][y + j] = p->sym;
			j++;
		}
		maxj = MAX(j, maxj);
		i++;
	}
	map->size = MAX(MAX(map->size, y + maxj), x + i);
}

void	erase_on_map(t_piece *p, int x, int y, t_map *map)
{
	int	i;
	int	j;

	i = 0;
	while (i < p->w)
	{
		j = 0;
		while (j < p->h)
		{
			if (p->grid[i][j] == FULL)
				map->grid[x + i][y + j] = EMPTY_SYM;
			j++;
		}
		i++;
	}
	map->size = map_size(map);
}

t_bool	can_put_on_map(t_piece *p, int x, int y, t_map *map)
{
	int	i;
	int	j;

	i = 0;
	while (i < p->w)
	{
		j = 0;
		while (j < p->h)
		{
			if (p->grid[i][j] == FULL && map->grid[x + i][y + j] != EMPTY_SYM)
				return (FALSE);
			j++;
		}
		i++;
	}
	return (TRUE);
}
