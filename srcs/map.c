/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etrobert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:25:26 by etrobert          #+#    #+#             */
/*   Updated: 2016/11/09 18:28:33 by etrobert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"

t_map	*create_map(int max_size)
{
	t_map	*ret;
	int		i;

	if ((ret = malloc(sizeof(*ret))) == NULL)
		return (NULL);
	ret->max_size = max_size;
	if ((ret->grid = malloc(sizeof(char*) * max_size)) == NULL)
		return (NULL);
	i = 0;
	while (i < max_size)
	{
		if ((ret->grid[i] = malloc(sizeof(char) * max_size)) == NULL)
			return (NULL);
		i++;
	}
	return (ret);
}

void	delete_map(t_map *map)
{
	int i;

	if (map == NULL)
		return ;
	i = 0;
	while (i < map->max_size)
	{
		free(map->grid[i]);
		i++;
	}
	free(map->grid);
	free(map);
}

void	init_map(t_map *map)
{
	int	i;
	int	j;

	map->size = 0;
	i = 0;
	while (i < map->max_size)
	{
		j = 0;
		while (j < map->max_size)
		{
			map->grid[i][j] = '.';
			j++;
		}
		i++;
	}
}

int		map_size(t_map *map)
{
	int	i;
	int	j;
	int tmp;

	tmp = 0;
	i = 0;
	while (i < map->max_size)
	{
		j = 0;
		while (j < map->max_size)
		{
			if (map->grid[i][j] != EMPTY_SYM)
				tmp = MAX(i + 1, j + 1);
			j++;
		}
		i++;
	}
	return (tmp);
}

/*
** The two maps have to have the same max_size
*/

void	map_cpy(t_map *dest, t_map *src)
{
	int	i;
	int	j;

	if (dest == NULL || src == NULL)
		return ;
	dest->size = src->size;
	i = 0;
	while (i < dest->max_size)
	{
		j = 0;
		while (j < dest->max_size)
		{
			dest->grid[i][j] = src->grid[i][j];
			j++;
		}
		i++;
	}
}
