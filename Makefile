NAME	= fillit

SRCDIR	= srcs
OBJDIR	= objs
INCDIR	= includes

SRCNAM	= main.c map.c map_utils.c piece.c print.c solve.c \
		  input.c valid.c piece_tools.c \
		  piece_size.c

SRC		= $(SRCNAM:%=$(SRCDIR)/%)
OBJ		= $(SRC:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

CC		= gcc
CFLAGS	= -Wall -Wextra -Werror -I$(INCDIR) -Ilibft
LDFLAGS	= -lft -Llibft

INCNAM	= map.h map_utils.h piece.h print.h solve.h \
		  input.h valid.h piece_tools.h piece_size.h

INC		= $(INCNAM:%=$(INCDIR)/%)
GIT		= Makefile

LIBFT	= libft/libft.a

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(LIBFT)
	mkdir -p $(OBJDIR)
	$(CC) -c $(CFLAGS) -o $@ $< 

$(LIBFT):
	$(MAKE) -C libft

.PHONY: all git no printf check clean fclean re 

git:
	$(MAKE) -C libft git
	git add $(SRC) $(INC) $(GIT)

no:
	@echo "Passage de la norminette :"
	@norminette $(SRC) $(INC)

printf:
	@echo "Detection des printf :\033[1;31m"
	@grep printf -r $(SRCDIR) $(INCDIR) | cat
	@printf "\033[0m"

check: no printf

clean:
	$(MAKE) -C libft clean
	rm -rf $(OBJDIR)

fclean: clean
	$(MAKE) -C libft fclean
	rm -rf $(NAME)

# $(MAKE) needed so that the cleaning is done before starting to create again \
	# cf make -j 
re: fclean
	$(MAKE) -C libft re
	$(MAKE) all
